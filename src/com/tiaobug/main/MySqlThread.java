package com.tiaobug.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.swing.JTextArea;

import org.apache.commons.io.FileUtils;


public class MySqlThread extends Thread{
		MySqlThread(JTextArea textArea,int mode,int operType) {
		this.jTextArea=textArea;
		this.mode=mode;
		this.operType=operType;
	}

	// 重载run函数
	public void run() {
		
		checkConfFile();
		int i = mode * operType;
		Runtime runtime = Runtime.getRuntime();
		try {
			this.setEnv(runtime);
		} catch (Exception e) {
			e.printStackTrace();
		}
		switch (i) {
		case 1:
			startService(runtime);
			break;
		case 2:
			stopService(runtime);
			break;
		case 3:
			installService(runtime);
			break;
		case 4:
			uninstallService(runtime);
			break;
		

		default:
			break;
		}
		
		
	}
	
	public void checkConfFile()
	{
		String configPath=currentPath+"\\mysql5\\my.ini";
		File f=new File(configPath);
		if (!f.exists()) {
			try {
				File f_lagre=new File(currentPath+"\\mysql5\\my-large.ini");
				List<String> l=(List<String>)FileUtils.readLines(f_lagre);
//				for (int i = 0; i < l.size(); i++) {
//					if (l.get(i).equals("[mysqld]")) {
//						l.add(i+10, "default-storage-engine=innodb");
//						l.add(i+10, "default-character-set=utf8");
//						l.add(i+10, "datadir=\""+currentPath.replace("\\", "/")+"/mysql5/data/\"");
//						l.add(i+10, "basedir=\""+currentPath.replace("\\", "/")+"/mysql5/\"");
//						break;
//					}
//				}
				FileUtils.writeLines(f, l);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				TiaobugJTM.allTextArea.append(currentPath+"\\mysql5\\my-large.ini  is not exist!" + "\n");
				TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
			}
		}
	}
	
	
	public void installService(Runtime runtime) {
		Process p = null;
		try {
			String cmds = currentPath + "\\mysql5\\bin\\mysqld --install TiaobugMysql5 --defaults-file="+currentPath.replace("\\", "/")+"/mysql5/my.ini ";
			System.out.println(""+cmds);
			p = runtime.exec("cmd /c  " + cmds);
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream(), "GB2312"));
			String currentLine = null;
			while ((currentLine = in.readLine()) != null) {
				jTextArea.append(currentLine + "\n");
				jTextArea.paintImmediately(jTextArea.getBounds());
				TiaobugJTM.allTextArea.append(currentLine + "\n");
				TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
			}
		} catch (Exception e) {
			e.printStackTrace();
			jTextArea.append(e.getMessage() + "\n");
			jTextArea.paintImmediately(jTextArea.getBounds());
			TiaobugJTM.allTextArea.append(e.getMessage() + "\n");
			TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
		}
	}

	public void uninstallService(Runtime runtime) {
		Process p = null;
		try {
			String cmds = currentPath + "\\mysql5\\bin\\mysqld --remove TiaobugMysql5  ";
			p = runtime.exec("cmd /c  " + cmds);
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream(), "GB2312"));
			String currentLine = null;
			while ((currentLine = in.readLine()) != null) {
				jTextArea.append(currentLine + "\n");
				jTextArea.paintImmediately(jTextArea.getBounds());
				TiaobugJTM.allTextArea.append(currentLine + "\n");
				TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
			}
		} catch (Exception e) {
			e.printStackTrace();
			jTextArea.append(e.getMessage() + "\n");
			jTextArea.paintImmediately(jTextArea.getBounds());
			TiaobugJTM.allTextArea.append(e.getMessage() + "\n");
			TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
		}
	}

	public void startService(Runtime runtime) {
		Process p = null;
		try {
			String cmds = "net start TiaobugMysql5";
			p = runtime.exec("cmd /c  " + cmds);
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream(), "GB2312"));
			String currentLine = null;
			while ((currentLine = in.readLine()) != null) {
				jTextArea.append(currentLine + "\n");
				jTextArea.paintImmediately(jTextArea.getBounds());
				TiaobugJTM.allTextArea.append(currentLine + "\n");
				TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			jTextArea.append(e.getMessage() + "\n");
			jTextArea.paintImmediately(jTextArea.getBounds());
			TiaobugJTM.allTextArea.append(e.getMessage() + "\n");
			TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
		}
	}

	public void stopService(Runtime runtime) {
		Process p = null;
		try {
			String cmds = "net stop TiaobugMysql5";
			p = runtime.exec("cmd /c  " + cmds);
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream(), "GB2312"));
			String currentLine = null;
			while ((currentLine = in.readLine()) != null) {
				jTextArea.append(currentLine + "\n");
				jTextArea.paintImmediately(jTextArea.getBounds());
				TiaobugJTM.allTextArea.append(currentLine + "\n");
				TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
			}
		} catch (Exception e) {
			e.printStackTrace();
			jTextArea.append(e.getMessage() + "\n");
			jTextArea.paintImmediately(jTextArea.getBounds());
			TiaobugJTM.allTextArea.append(e.getMessage() + "\n");
			TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
		}
	}


	
	public void setEnv(Runtime runtime) throws Exception {

		String setJavaEnv="cmd /c SET JAVA_HOME=" + PathUtil.getPath() + "\\jre7";
		String setTomcatEnv="cmd /c SET CATALINA_HOME=" + PathUtil.getPath() + "\\tomcat7";
		runtime.exec(setJavaEnv);
		runtime.exec(setTomcatEnv);
	}

	private JTextArea jTextArea;
	private int mode;
	private int operType;

	public static final int MODE_SERVICE=1;
	public static final int MODE_DEBUG=2;
	public static final int OPER_TYPE_START=1;
	public static final int OPER_TYPE_STOP=2;
	public static final int OPER_TYPE_INSTALL=3;
	public static final int OPER_TYPE_UNINSTALL=4;
	
	
	private String currentPath = PathUtil.getPath();

	
}
