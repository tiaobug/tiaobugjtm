package com.tiaobug.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 
 * @author Tiaobug
 * @since 2013年1月2日
 */
public class TiaobugJTM extends JFrame implements ActionListener {

	public TiaobugJTM() {
		initComponents();
	}

	public static void main(String args[]) throws Exception {

		UIManager.setLookAndFeel(UIManager.getInstalledLookAndFeels()[3].getClassName());

		new TiaobugJTM();

	}

	private void initComponents() {

		serviceStartButton = new JButton("serviceStartButton");
		serviceStopButton = new JButton("serviceStopButton");
		debugStartButton = new JButton("debugStartButton");
//		debugStopButton = new JButton("debugStopButton");
		serviceInstallButton = new JButton("serviceInstallButton");
		serviceUninstallButton = new JButton("serviceUninstallButton");

		serviceStartButton.addActionListener(this);
		serviceStopButton.addActionListener(this);
		debugStartButton.addActionListener(this);
//		debugStopButton.addActionListener(this);
		serviceInstallButton  .addActionListener(this);
		serviceUninstallButton.addActionListener(this);

		serviceStartButton.setText("启动(服务)");
		serviceStopButton.setText("停止(服务)");
		debugStartButton.setText("启动(调试)");
//		debugStopButton.setText("停止(调试)");
		serviceInstallButton.setText("安装(服务)");
		serviceUninstallButton.setText("卸载(服务)");
	

		buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 1, 4));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

//		buttonPanel.add(debugStopButton, FlowLayout.LEFT);
		buttonPanel.add(debugStartButton, FlowLayout.LEFT);
		buttonPanel.add(serviceStopButton, FlowLayout.LEFT);
		buttonPanel.add(serviceStartButton, FlowLayout.LEFT);
		buttonPanel.add(serviceUninstallButton, FlowLayout.LEFT);
		buttonPanel.add(serviceInstallButton, FlowLayout.LEFT);

		stateTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tomcatStatePane = new JScrollPane();
		mysqlStatePane = new JScrollPane();
		allStatePane = new JScrollPane();
		helpPane = new JScrollPane(helpTextArea);

		helpTextArea = new JTextArea();
		helpTextArea.setLineWrap(true);// 激活自动换行功能
		helpTextArea.setWrapStyleWord(true);// 激活断行不断字功能
		mysqlTextArea = new JTextArea();
		mysqlTextArea.setLineWrap(true);// 激活自动换行功能
		mysqlTextArea.setWrapStyleWord(true);// 激活断行不断字功能
		tomcatTextArea = new JTextArea();
		tomcatTextArea.setLineWrap(true);// 激活自动换行功能
		tomcatTextArea.setWrapStyleWord(true);// 激活断行不断字功能
		allTextArea = new JTextArea();
		allTextArea.setLineWrap(true);// 激活自动换行功能
		allTextArea.setWrapStyleWord(true);// 激活断行不断字功能

		allStatePane.setBorder(BorderFactory.createEtchedBorder());
		stateTabbedPane.addTab("运行状态", allStatePane);
		allStatePane.setViewportView(allTextArea);
		tomcatStatePane.setBorder(BorderFactory.createEtchedBorder());
		stateTabbedPane.addTab("Tomcat状态", tomcatStatePane);
		tomcatStatePane.setViewportView(tomcatTextArea);
		mysqlStatePane.setBorder(BorderFactory.createEtchedBorder());
		stateTabbedPane.addTab("Mysql状态", mysqlStatePane);
		mysqlStatePane.setViewportView(mysqlTextArea);
		helpPane.setBorder(BorderFactory.createEtchedBorder());
		stateTabbedPane.addTab("帮助", helpPane);
		helpPane.setViewportView(helpTextArea);
		loadHlepInfo(helpTextArea);
		

		Container c = getContentPane();

		c.add(buttonPanel, BorderLayout.NORTH);
		c.add(stateTabbedPane, BorderLayout.CENTER);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setSize(550, 500);
		setVisible(true);
		setTitle("条八哥 Java Web 一键部署小软件 （www.tiaobug.com）");
		// pack();
	}

	public void loadHlepInfo(JTextArea helpArea)
	{
		File helpTxt=new File(PathUtil.getPath()+"\\Help.txt");
		try {
			List<String> l=(List<String>)FileUtils.readLines(helpTxt, "UTF-8");
			for (int i = 0; i < l.size(); i++) {
				helpArea.append(l.get(i)+"\n");
			}
			helpArea.paintImmediately(helpArea.getBounds());

		} catch (IOException e) {
			helpArea.append(PathUtil.getPath()+"\\Help.txt  is not exist!" + "\n");
			helpArea.paintImmediately(helpArea.getBounds());
		}
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			String cmcString = e.getActionCommand();
			if (cmcString.equals("安装(服务)")) {
				MySqlThread mySqlThread = new MySqlThread(mysqlTextArea, MySqlThread.MODE_SERVICE, MySqlThread.OPER_TYPE_INSTALL);
				mySqlThread.start();
				TomcatThread tomcatThread = new TomcatThread(tomcatTextArea, TomcatThread.MODE_SERVICE, TomcatThread.OPER_TYPE_INSTALL);
				tomcatThread.start();
			} else if (cmcString.equals("卸载(服务)")) {
				MySqlThread mySqlThread = new MySqlThread(mysqlTextArea, MySqlThread.MODE_SERVICE, MySqlThread.OPER_TYPE_UNINSTALL);
				mySqlThread.start();
				TomcatThread tomcatThread = new TomcatThread(tomcatTextArea, TomcatThread.MODE_SERVICE, TomcatThread.OPER_TYPE_UNINSTALL);
				tomcatThread.start();
			} else if (cmcString.equals("启动(服务)")) {
//				MySqlThread mySqlThread = new MySqlThread(mysqlTextArea, MySqlThread.MODE_SERVICE, MySqlThread.OPER_TYPE_START);
//				mySqlThread.start();
				TomcatThread tomcatThread = new TomcatThread(tomcatTextArea, TomcatThread.MODE_SERVICE, TomcatThread.OPER_TYPE_START);
				tomcatThread.start();
			} else if (cmcString.equals("停止(服务)")) {
//				MySqlThread mySqlThread = new MySqlThread(mysqlTextArea, MySqlThread.MODE_SERVICE, MySqlThread.OPER_TYPE_STOP);
//				mySqlThread.start();
				TomcatThread tomcatThread = new TomcatThread(tomcatTextArea, TomcatThread.MODE_SERVICE, TomcatThread.OPER_TYPE_STOP);
				tomcatThread.start();
			} else if (cmcString.equals("启动(调试)")) {
				TomcatThread tomcatThread = new TomcatThread(tomcatTextArea, TomcatThread.MODE_DEBUG, TomcatThread.OPER_TYPE_START);
				tomcatThread.start();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	
	private JButton serviceStartButton;
	private JButton serviceStopButton;
	private JButton serviceInstallButton;
	private JButton serviceUninstallButton;
	private JButton debugStartButton;
	private JButton debugStopButton;
	private JScrollPane allStatePane;// 显示主程序装
	private JScrollPane tomcatStatePane;// 显示TOMCAT状态
	private JScrollPane mysqlStatePane;// 显示MySql状态
	private JScrollPane helpPane;// 显示帮组
	private JTabbedPane stateTabbedPane;
	private JPanel buttonPanel;

	private JTextArea helpTextArea;
	private JTextArea mysqlTextArea;
	private JTextArea tomcatTextArea;
	public static JTextArea allTextArea;
}
