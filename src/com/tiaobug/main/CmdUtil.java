package com.tiaobug.main;


public class CmdUtil extends Thread {

	public void run() {
		Process p = null;
		try {
			p = Runtime.getRuntime().exec("cmd /c  " + cmdString);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Process run(String cmd) throws Exception {
		return Runtime.getRuntime().exec("cmd /c  " + cmd);
	}

	public CmdUtil(String cmdString) {
		this.cmdString = cmdString;
	}

	private String cmdString;

}
