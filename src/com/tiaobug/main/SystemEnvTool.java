package com.tiaobug.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.swing.JFrame;

public class SystemEnvTool  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8033647985281792865L;


	public Properties getEnv() throws Exception {
		Properties prop = new Properties();
		String OS = System.getProperty("os.name").toLowerCase();
		Process p = null;
		if (OS.indexOf("windows") > -1) {
			p = Runtime.getRuntime().exec("cmd /k set");
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while ((line = br.readLine()) != null) {
			int i = line.indexOf("=");
			if (i > -1) {
				String key = line.substring(0, i);
				String value = line.substring(i + 1);
				prop.setProperty(key, value);
			}
		}
		return prop;
	}

	// 设置java运行环境
	public static void setJavaEnv() {
		currentPath = PathUtil.getPath();
		cmdString = "SET JAVA_HOME ='" + currentPath + "\\jre7\\'";
		CmdUtil cu = new CmdUtil(cmdString);
		cu.start();
	}

	// 设置Tomcat运行环境
	public static void setTomcatEnv() {
		currentPath = PathUtil.getPath();
		cmdString = "SET CATALINA_HOME ='" + currentPath + "\\tomcat7\\'";
		CmdUtil cu = new CmdUtil(cmdString);
		cu.start();
	}

	
	public static void setEnv() {
		currentPath = PathUtil.getPath();
		cmdString = currentPath + "\\setEnv.bat";
		CmdUtil cu = new CmdUtil(cmdString);

	}
	
	private static String currentPath;
	private static String cmdString;
}
