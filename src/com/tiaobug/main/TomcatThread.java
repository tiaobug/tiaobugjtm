package com.tiaobug.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.swing.JTextArea;

public class TomcatThread extends Thread {

	TomcatThread(JTextArea textArea, int mode, int operType) {
		this.jTextArea = textArea;
		this.mode = mode;
		this.operType = operType;
	}

	// 重载run函数
	@Override
	public void run() {
		int i = mode * operType;
		Runtime runtime = Runtime.getRuntime();
		try {
			this.setEnv(runtime);
		} catch (Exception e) {
			e.printStackTrace();
		}
		switch (i) {
		case 1:
			installService(runtime);
			break;
		case 2:
			uninstallService(runtime);
			break;
		case 3:
			startService(runtime);
			break;
		case 4:
			stopService(runtime);
			break;
		case 6:
			startBatDebug(runtime);
			break;
//		case 8:
//			stopBatDebug(runtime);
//			break;
		default:
			break;
		}
	}

	public void installService(Runtime runtime) {
		Process p = null;
		try {
			String cmds = currentPath + "\\tomcat7\\bin\\service.bat install TiaobugTomcat7";
			p = runtime.exec("cmd /c  " + cmds);
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream(), "GB2312"));
			String currentLine = null;
			while ((currentLine = in.readLine()) != null) {
				jTextArea.append(currentLine + "\n");
				jTextArea.paintImmediately(jTextArea.getBounds());
				TiaobugJTM.allTextArea.append(currentLine + "\n");
				TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
			}
		} catch (Exception e) {
			e.printStackTrace();
			jTextArea.append(e.getMessage() + "\n");
			jTextArea.paintImmediately(jTextArea.getBounds());
			TiaobugJTM.allTextArea.append(e.getMessage() + "\n");
			TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
		}
	}

	public void uninstallService(Runtime runtime) {
		Process p = null;
		try {
			String cmds = currentPath + "\\tomcat7\\bin\\service.bat remove TiaobugTomcat7";
			p = runtime.exec("cmd /c  " + cmds);
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream(), "GB2312"));
			String currentLine = null;
			while ((currentLine = in.readLine()) != null) {
				jTextArea.append(currentLine + "\n");
				jTextArea.paintImmediately(jTextArea.getBounds());
				TiaobugJTM.allTextArea.append(currentLine + "\n");
				TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
			}
		} catch (Exception e) {
			e.printStackTrace();
			jTextArea.append(e.getMessage() + "\n");
			jTextArea.paintImmediately(jTextArea.getBounds());
			TiaobugJTM.allTextArea.append(e.getMessage() + "\n");
			TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
		}
	}

	public void startService(Runtime runtime) {
		Process p = null;
		try {
			String cmds = "sc start TiaobugTomcat7";
			p = runtime.exec("cmd /c  " + cmds);
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream(), "GB2312"));
			String currentLine = null;
			while ((currentLine = in.readLine()) != null) {
				jTextArea.append(currentLine + "\n");
				jTextArea.paintImmediately(jTextArea.getBounds());
				TiaobugJTM.allTextArea.append(currentLine + "\n");
				TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			jTextArea.append(e.getMessage() + "\n");
			jTextArea.paintImmediately(jTextArea.getBounds());
			TiaobugJTM.allTextArea.append(e.getMessage() + "\n");
			TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
		}
	}

	public void stopService(Runtime runtime) {
		Process p = null;
		try {
			String cmds = "sc stop TiaobugTomcat7";
			p = runtime.exec("cmd /c  " + cmds);
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream(), "GB2312"));
			String currentLine = null;
			while ((currentLine = in.readLine()) != null) {
				jTextArea.append(currentLine + "\n");
				jTextArea.paintImmediately(jTextArea.getBounds());
				TiaobugJTM.allTextArea.append(currentLine + "\n");
				TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
			}
		} catch (Exception e) {
			e.printStackTrace();
			jTextArea.append(e.getMessage() + "\n");
			jTextArea.paintImmediately(jTextArea.getBounds());
			TiaobugJTM.allTextArea.append(e.getMessage() + "\n");
			TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
		}
	}

	public void startBatDebug(Runtime runtime) {
		Process p = null;
		try {
//			p=runtime.exec("cmd /c set JAVA_HOME");
			String cmds = currentPath + "\\tomcat7\\bin\\startup.bat";
			p = runtime.exec("cmd /c  " + cmds);
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream(), "GB2312"));
			String currentLine = null;
			while ((currentLine = in.readLine()) != null) {
				jTextArea.append(currentLine + "\n");
				jTextArea.paintImmediately(jTextArea.getBounds());
				TiaobugJTM.allTextArea.append(currentLine + "\n");
				TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
			}
		} catch (Exception e) {
			e.printStackTrace();
			jTextArea.append(e.getMessage() + "\n");
			jTextArea.paintImmediately(jTextArea.getBounds());
			TiaobugJTM.allTextArea.append(e.getMessage() + "\n");
			TiaobugJTM.allTextArea.paintImmediately(jTextArea.getBounds());
		}
	}


	public void setEnv(Runtime runtime) throws Exception {

		String setJavaEnv="cmd /c SET JAVA_HOME=" + PathUtil.getPath() + "\\jre7";
		String setTomcatEnv="cmd /c SET CATALINA_HOME=" + PathUtil.getPath() + "\\tomcat7";
		runtime.exec(setJavaEnv);
		runtime.exec(setTomcatEnv);

	}

	private JTextArea jTextArea;
	private int mode;
	private int operType;
	public static final int MODE_SERVICE = 1;
	public static final int MODE_DEBUG = 2;
	public static final int OPER_TYPE_INSTALL = 1;
	public static final int OPER_TYPE_UNINSTALL = 2;
	public static final int OPER_TYPE_START = 3;
	public static final int OPER_TYPE_STOP = 4;
	private String currentPath = PathUtil.getPath();
}
