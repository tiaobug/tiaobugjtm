﻿<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page session="false" %>
<%
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");
request.setAttribute("year", sdf.format(new java.util.Date()));
request.setAttribute("tomcat7Url", "http://tomcat.apache.org/");
request.setAttribute("tomcat7DocUrl", "/docs/");
request.setAttribute("tomcat7ExamplesUrl", "/examples/");
%>
<html>
    <head>
        <title><%=request.getServletContext().getServerInfo() %></title>
        <link href="favicon.ico" rel="icon" type="image/x-icon" />
        <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <link href="tomcat.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <div id="wrapper">
            <div id="asf-box">
                <h1>${pageContext.servletContext.serverInfo}</h1>
            </div>
            <div id="upper" class="curved container">
                <div id="congrats" class="curved container">
                    <h2>If you're seeing this, you've successfully installed Tomcat. Congratulations!</h2>
                    <h2>如果你看到这个页面，表示安装TOMCAT成功了，恭喜！</h2>
                    <h2>Put your web application here!</h2>
                    <h2>把你的web应用程序放到这里就可以了！</h2>
                     <h3>path: {install patt}/tomcat7/webapps/ROOT/</h3>
                </div>
            </div>          
        </div>
    </body>

</html>
